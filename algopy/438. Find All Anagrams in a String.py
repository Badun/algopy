"""
https://leetcode.com/problems/find-all-anagrams-in-a-string/
"""
from typing import List


class Solution:
    """
    Given two strings s and p, return an array of all the start indices of p's anagrams in s.
    You may return the answer in any order.

    An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase,
    typically using all the original letters exactly once.
    """
    def findAnagrams(self, s: str, p: str) -> List[int]:
        S = len(s)
        P = len(p)

        pattern = [0] * 26
        lookup = [0] * 26
        for c in p:
            lookup[ord(c) - ord('a')] += 1

        res = []

        start = 0
        end = 0
        while end < S:
            lookup[ord(s[end]) - ord('a')] -= 1
            end += 1

            if end - start < P:
                continue

            if lookup == pattern:
                res.append(start)

            lookup[ord(s[start]) - ord('a')] += 1
            start += 1

        return res
