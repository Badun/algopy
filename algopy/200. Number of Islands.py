import itertools
from collections import Counter
from typing import List


class Solution:
    """
    Use Union-Find Structure.

    Note: if allowed to change grid, you can just remove islands from the grid if you see them.
    """

    def numIslands(self, grid: List[List[str]]) -> int:
        M = len(grid)
        N = len(grid[0])

        link = [
            i for i in range(M * N)
        ]
        size = [1] * (M * N)
        res = Counter(itertools.chain(*grid))['1']

        def pos(r, c):
            return r * N + c

        def find(p):
            if link[p] == p:
                return p
            link[p] = find(link[p])
            return link[p]

        def same(r1, c1, r2, c2):
            return find(pos(r1, c1)) == find(pos(r2, c2))

        def merge(r1, c1, r2, c2):
            nonlocal res
            res -= 1

            parent1 = find(pos(r1, c1))
            parent2 = find(pos(r2, c2))

            if size[parent1] > size[parent2]:
                parent1, parent2 = parent2, parent1

            link[parent2] = parent1
            size[parent1] += size[parent2]

        def dfs(r, c):
            if grid[r][c] == '0':
                return

            for ri, ci in [(r - 1, c), (r + 1, c), (r, c - 1), (r, c + 1)]:
                if ri < 0 or ri >= M or ci < 0 or ci >= N:
                    continue

                print(ri, ci)
                if grid[ri][ci] == '0':
                    continue

                if same(r, c, ri, ci):
                    continue

                merge(r, c, ri, ci)
                dfs(ri, ci)

        for r in range(M):
            for c in range(N):
                dfs(r, c)

        return res
