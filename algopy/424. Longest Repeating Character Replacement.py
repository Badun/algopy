"""
https://leetcode.com/problems/longest-repeating-character-replacement/

You are given a string s and an integer k.
You can choose any character of the string and change it to any other uppercase English character.
You can perform this operation at most k times.

Return the length of the longest substring containing the same letter you can get after performing the above operations.
"""
import collections


class Solution:
    def characterReplacement(self, s, k):
        max_freq, res = 0, 0
        counter = collections.Counter()
        for i in range(len(s)):
            counter[s[i]] += 1
            max_freq = max(max_freq, counter[s[i]])
            if res - max_freq < k:
                res += 1
            else:
                counter[s[i - res]] -= 1
        return res
